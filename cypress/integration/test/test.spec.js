/// <reference types="Cypress" />
import { 
    loginPageElements,
    homePageElements,
    cartPageElements
} from '../../pageLocators/generalLocators.locators'

describe ('Purchase products at SwagLabs', () => {

    before(() => {
        cy.visit('')
      })

    beforeEach(() => {
        Cypress.Cookies.preserveOnce('session-username')  
    })

    it('User Authentication fail - Login on Swag Labs page', () => {
        cy.get(loginPageElements.userAccountTitle).should('be.visible')
        cy.get(loginPageElements.usernameField).should('be.visible').type('standard_user')
        cy.get(loginPageElements.passwordField).should('be.visible').type('secret')
        cy.get(loginPageElements.buttonLogin).should('be.visible').click()
        cy.get(loginPageElements.errorMessage).should('contain' , 'Epic sadface: Username and password do not match any user in this service')
    })

    it('User Authentication successful - Login on Swag Labs page', () => {
        cy.get(loginPageElements.passwordField).should('be.visible').clear().type('secret_sauce')
        cy.get(loginPageElements.buttonLogin).should('be.visible').click()
        cy.get(homePageElements.menuContainer).should('contain' , 'Products')
    })

    it('Add Products to the cart', () => {
        cy.get(homePageElements.sortContainer).select(['Price (low to high)'])
        cy.get(homePageElements.addProduct).eq(0).click()       
        cy.get(homePageElements.sortContainer).select(['Price (high to low)'])
        cy.get(homePageElements.addProduct).eq(0).click()  
    })

    it('Shopping cart', () => {
        cy.get(homePageElements.shoppingCart).click()
        cy.get(homePageElements.menuContainer).should('contain' , 'Your Cart')  
        cy.get(cartPageElements.checkout).click()
        cy.get(cartPageElements.checkoutInfo).should('be.visible')
            cy.fixture('checkoutData.json').then(data => {   
               cy.get(cartPageElements.firstName).type(data.firstName)
               cy.get(cartPageElements.lastName).type(data.lastName)
               cy.get(cartPageElements.zipCode).type(data.zipCode)
            })
        cy.get(cartPageElements.continueButton).click()  
        cy.get(homePageElements.menuContainer).should('contain' , 'Checkout: Overview')  
        cy.get(cartPageElements.finish).click()  
        cy.get(homePageElements.menuContainer).should('contain' , 'Checkout: Complete!')  
        cy.get(cartPageElements.checkoutMessage).and('contain', 'THANK YOU FOR YOUR ORDER') 
    })
   
    
})
