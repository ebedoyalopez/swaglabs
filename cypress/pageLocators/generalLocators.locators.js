export const loginPageElements = {
    userAccountTitle: '#login_button_container',
    usernameField: '#user-name',
    passwordField: '#password',
    buttonLogin: '#login-button',
    errorMessage: '.error-message-container.error'
}

export const homePageElements = {
    menuContainer: '.title',    
    sortContainer: '.product_sort_container',
    addProduct: '.inventory_list .inventory_item .pricebar .btn',
    shoppingCart: '#shopping_cart_container .shopping_cart_link',
}

export const cartPageElements = {
    checkout: '#checkout',  
    checkoutInfo: '.checkout_info',  
    firstName: '#first-name',
    lastName: '#last-name',
    zipCode: '#postal-code',
    continueButton: '#continue',
    finish: '#finish',
    checkoutMessage: '#checkout_complete_container',
    facebook: '.social_facebook'
}


